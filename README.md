# TestBlog
Um projeto com objetivo de explorar as possibilidades do ORM [prisma](https://www.prisma.io/).
### Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), [Docker](https://www.docker.com/get-started), [Docker-Compose](https://docs.docker.com/compose/install/). 
Além disto é bom ter um editor para trabalhar com o código como [VSCode](https://code.visualstudio.com/)

### Como Rodar

```bash
# Clone este repositório
$ git clone https://gitlab.com/Def4ult/testblog.git

# Acesse a pasta do projeto no terminal/cmd
$ cd testblog

# Copie o arquivo .env_example para .env
$ cp .env.example .env

# Rode os serviços com docker-compose
$ docker-compose up -d

# O banco inciará na porta:5432 - <http://localhost:5432>
